require 'tempfile'
require 'fileutils'

class ProxyFile
  def initialize(proxy)
    @proxy = proxy
    Tempfile.create('proxy') do |f|
      f.write(@proxy)
      f.flush
      FileUtils.chmod('u=wr,go=', f)
      proxy_path = File.absolute_path(f)
      yield(proxy_path)
    end
  end
end
