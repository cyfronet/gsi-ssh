# Another timer option with Kernel usage

require 'open3'

BUFFER_SIZE = 4096
def run_with_timeout(command, timeout)
  output = ''
  error_output = ''
  begin
    stdin, stdout, stderr, thread = Open3.popen3(command)
    pid = thread[:pid]
    start = Time.now

    while ((Time.now - start) < timeout) && thread.alive?
      Kernel.select([stdout, stderr], nil, nil, timeout)
      begin
        output << stdout.read_nonblock(BUFFER_SIZE)
        error_output << stderr.read_nonblock(BUFFER_SIZE)
      rescue IO::WaitReadable
        # skip
      rescue EOFError
        break
      end
    end
    sleep 1

    if thread.alive?
      Process.kill('TERM', pid)
      output += 'Process completed!'
    end
  ensure
    stdin.close if stdin
    stdout.close if stdout
    stderr.clone if stderr
  end
  return error_output if output != ''

  output
end

# timer done with non-blocking read
Open3.popen3({ 'X509_USER_PROXY' => proxy_path }, *full_command) do |_stdin, stdout, stderr, wait_thr|
  popen_pid = wait_thr.pid
  begin
    Timeout.timeout(timeout) do
      exit_loop = false

      until exit_loop
        stdout_eof = false
        stderr_eof = false

        begin
          line = stdout.read_nonblock(128)
          puts '--out--'
          puts line
          output += line
        rescue IO::WaitReadable
        # Ignored
        rescue EOFError
          stdout_eof = true
        end

        begin
          line = stderr.read_nonblock(128)
          puts '--err--'
          puts line
          error_output += line
        rescue IO::WaitReadable
        # Ignored
        rescue EOFError
          stderr_eof = true
        end

        if stdout_eof && stderr_eof
          exit_loop = true
          puts 'EXITING'
        end
      end
    end
  rescue Timeout::Error
    # TODO: this exception is raised when connection takes too long or executing command takes too long
    raise Timeout::Error
  ensure
    begin
      Process.kill('KILL', popen_pid) if popen_pid != -1
    rescue Errno::ESRCH
      # Ignored
    end
  end
end
