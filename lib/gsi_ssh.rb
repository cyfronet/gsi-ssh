# frozen_string_literal: true

require 'open3'
require 'timeout'
require_relative 'separator_generator'
require_relative 'proxy_file'

class GsiSsh
  class Error < ::StandardError
  end

  class WrongHostException < Error
  end

  class ParseException < Error
  end

  def initialize(proxy, host: 'pro.cyfronet.pl')
    begin
      validate_host(host)
    rescue WrongHostException
      raise WrongHostException
    end

    @proxy = proxy
    @host = host
  end

  def run(command, timeout: 5)
    output = ''
    error_output = ''
    separator = SeparatorGenerator.get_separator(command)

    ProxyFile.new(@proxy) do |proxy_path|
      full_command = complete_command(command, separator)

      stdin, stdout, stderr, thread = Open3.popen3({ 'X509_USER_PROXY' => proxy_path }, *full_command)
      popen_pid = thread.pid
      stdin.close

      begin
        output, error_output = timer(stdout, stderr, thread, timeout)
      rescue Timeout::Error
        # TODO: this exception is raised when connection takes too long or executing command takes too long
        raise Timeout::Error
      ensure
        begin
          Process.kill('KILL', popen_pid)
        rescue Errno::ESRCH
          # Ignored
        end
      end
    end

    begin
      result = parse_output(output, error_output, separator)
    rescue ParseException
      # TODO: this exception is raised when wrong connection / exit during executing some remote command
      raise ParseException
    end
    result
  end

  private

  def validate_host(host)
    raise WrongHostException if host.include?(' ') || host[-1] == ';'
  end

  def complete_command(command, separator)
    gsissh_cmd = <<~COMMAND_EOF.strip.gsub(/\s+/, ' ')
      echo '#{separator}';
      #{command};
      echo '#{separator}';
      echo $?;
      echo '#{separator}'
    COMMAND_EOF

    ['gsissh',
     @host.to_s,
     '-v',
     '-o PreferredAuthentications=gssapi-keyex',
     '-o LogLevel=error',
     '-o StrictHostKeyChecking=no',
     gsissh_cmd]
  end

  def timer(stdout, stderr, thread, timeout)
    queue = Queue.new

    timer_thread = Thread.new do
      sleep timeout
      queue << 0 if thread.alive?
    end

    popen_thread = Thread.new do
      output = stdout.read
      error_output = stderr.read
      queue << [output, error_output]
    end

    value = queue.pop

    Thread.kill(timer_thread)
    Thread.kill(popen_thread)

    raise Timeout::Error if value == 0 # when timeout exceeded raise timeout error

    value
  end

  def parse_output(output, error_output, separator)
    start = output.index(separator)
    raise ParseException if start.nil?

    middle = output[(start + separator.length)..].index(separator)
    raise ParseException if middle.nil?

    middle += start + separator.length
    last = output[(middle + separator.length)..].index(separator)
    raise ParseException if last.nil?

    last += middle + separator.length

    parsed_output = output[(start + separator.length)..(middle - 1)].strip
    exit_status = output[(middle + separator.length)..(last - 1)].strip.to_i
    GsiRunResult.new(parsed_output, error_output.strip, exit_status)
  end
end
