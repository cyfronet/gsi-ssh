# frozen_string_literal: true

class GsiRunResult
  attr_reader :stdout, :stderr, :exit_status

  def initialize(output, error, exit_status)
    @stdout = output
    @stderr = error
    @exit_status = exit_status
  end

  def to_s
    "===================\n" \
      "--Gsi Run Results--\n" \
      "===================\n" \
      "stdout is:\n" +
      @stdout +
      "===================\n" \
      "stderr is:\n" +
      @stderr +
      "===================\n" \
      "exit status is:\n" +
      @exit_status.to_s +
      "\n==================="
  end
end
