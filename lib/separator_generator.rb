require 'securerandom'

class SeparatorGenerator
  class << SeparatorGenerator
    def get_separator(*args)
      sep = SecureRandom.uuid
      sep = SecureRandom.uuid while args.any? { |a| a.include?(sep) }
      sep
    end
  end
end
