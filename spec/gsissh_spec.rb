# frozen_string_literal: true

require 'gsi_ssh'
require 'gsi_run_result'

RSpec.describe GsiSsh do
  let(:wait_thr) { double }
  let(:stdin) { double }
  let(:stdout) { double }
  let(:stderr) { double }
  let(:separator_generator) { double }

  subject { described_class.new('proxy payload') }

  context 'tests' do
    context 'stdout / stderr tests' do
      before(:each) do
        allow(SeparatorGenerator).to receive(:get_separator).and_return('AAA', 'BBB')
        allow(stdin).to receive(:close)
        allow(wait_thr).to receive(:pid).and_return(nil)
        allow(Open3).to receive(:popen3).and_return([stdin, nil, nil, wait_thr])
        allow(Process).to receive(:kill)
      end

      it 'capture stdout and stderr' do
        # before each code
        allow(subject).to receive(:timer).and_return(["fooAAAstdout output\nAAA0\n#AAAbar", 'stderr output'])

        result = subject.run('echo lol')

        expect(result.exit_status).to eq 0
        expect(result.stdout).to eq 'stdout output'
        expect(result.stderr).to eq 'stderr output'
      end

      it 'capture empty stdout and stderr' do
        # before each code
        allow(subject).to receive(:timer).and_return(["fooAAA\nAAA0\n#AAAbar", ''])

        result = subject.run('echo lol')

        expect(result.exit_status).to eq 0
        expect(result.stdout).to eq ''
        expect(result.stderr).to eq ''
      end

      it 'capture nonzero exit status' do
        # before each code
        allow(subject).to receive(:timer).and_return(["fooAAA\nAAA173\n#AAAbar", ''])

        result = subject.run('echo lol')

        expect(result.exit_status).to eq 173
        expect(result.stdout).to eq ''
        expect(result.stderr).to eq ''
      end

      it 'raises ParseException on wrong connection' do
        # before each code
        allow(subject).to receive(:timer).and_return(["fooAAA\n173\n#AAAbar", ''])

        expect { subject.run('echo lol') }.to raise_error(GsiSsh::ParseException)
      end
    end

    it 'raises WrongHostException on wrong host' do
      expect { GsiSsh.new('payload', host: 'pro.cyfronet.pl exit') }.to raise_error(GsiSsh::WrongHostException)
      expect { GsiSsh.new('payload', host: 'pro.cyfronet.pl;') }.to raise_error(GsiSsh::WrongHostException)
    end

    context 'timeout tests' do
      let(:queue) { double }

      before(:each) do
        allow(SeparatorGenerator).to receive(:get_separator).and_return('AAA')
        allow(stdin).to receive(:close)

        allow(wait_thr).to receive(:alive?).and_return(true)

        allow(wait_thr).to receive(:pid).and_return(nil)

        allow(Open3).to receive(:popen3).and_return([stdin, stdout, stderr, wait_thr])
        allow(Process).to receive(:kill)

        allow(subject).to receive(:parse_output)
      end

      it 'raises Timeout::Error on timeout exceeded' do
        allow(stdout).to receive(:read) {
          sleep 1
          'output'
        }
        allow(stderr).to receive(:read) {
          sleep 1
          'error output'
        }

        expect { subject.run('echo lol', timeout: 1) }.to raise_error(Timeout::Error)
      end
      it 'deoesnt raise Timeout::Error when timeout didnt exceed' do
        allow(stdout).to receive(:read) {
          sleep 1
          'output'
        }
        allow(stderr).to receive(:read) {
          sleep 1
          'error output'
        }

        expect { subject.run('echo lol') }.not_to raise_error
      end
    end

    it 'creates proxy file with correct content and permissions' do
      ProxyFile.new('payload') do |path|
        expect(File.read(path)).to eq 'payload'
        expect(File.stat(path).mode.to_s(8)[3..5]).to eql '600'
      end
    end
  end
end
