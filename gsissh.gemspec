# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'gsissh'
  spec.version       = '0.1.0'
  spec.authors       = ['Wojciech Klyszejko']
  spec.email         = ['wojkly@wojkly-mint@mail.com']

  spec.summary       = 'gsi-ssh library in Ruby programming language'
  spec.description   = 'gsi-ssh library in Ruby programming language allowing to use commands on a remote machine'
  spec.homepage      = 'https://gitlab.com/cyfronet/gsi-ssh'
  spec.required_ruby_version = '>= 3.0.1'

  spec.metadata['allowed_push_host'] = "TODO: Set to 'https://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/cyfronet/gsi-ssh'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/cyfronet/gsi-ssh/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
