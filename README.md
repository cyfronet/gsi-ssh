# GSI-SSH gem


## Project description
A library (gem) that allows you to run commands on host using a SSH client with support for GSI authentication and credential forwarding in a secure way. At the beginning the gem was created that contain Ruby code and is packed with extra data such as lib folder, spec folder in which tests are located, Gemfile or Rakefile.


## Requirements
 - Linux distribution based on debain format (Ubuntu, Linux Mint)
 - Ruby 3.0.1 version or newer
 - grid CA certificate
 - gsi-ssh client
 - bundler

## Instalation process
### 1) Clone the repo
```
git clone https://gitlab.com/cyfronet/gsi-ssh.git
```

### 2) Ruby 3.0.1 version or newer
In order to install Ruby 3.0.1 or newer version you can use asdf manager:
```
sudo apt update
sudo apt install git
git clone https://github.com/asdf-vm/asdf.git ~/.asdf
```
Now you should add the following to the ~/.bashrc
```
. $HOME/.asdf/asdf.sh
. $HOME/.asdf/completions/asdf.bash
```
Then restart your shell so that PATH changes take effect.
Now you have to install the plugin.
```
cd ~/.asdf
git checkout "$(git describe --abbrev=0 --tags)"
asdf plugin-add ruby
```
Now you need to install ruby dependencies:
```
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev software-properties-common libffi-dev
```
Last thing to do is Ruby instalation:
```
asdf install ruby 3.0.1
```
If you will have any troubles in installing ruby or asdf visit:

http://asdf-vm.com/guide/getting-started.html#_1-install-dependencies

### 3) Gsi-ssh client
GSI-SSH (also gsi-openssh) is the GSI enabled OpenSSH Client and Server. To install gsi-ssh client you will nedd to run the following commands:
```
sudo apt-get update
sudo apt-get install globus-gsi
sudo apt-get install globus-data-management-client
sudo apt-get install myproxy
sudo apt install gsi-openssh-clients
```

### 4) Bundler
Bundler provides a consistent environment for Ruby projects by tracking and installing the exact gems and versions that are needed. In order to install bundler run the following command:

```
gem install bundler
```

If you have already bundler installed update it to the newest version:
```
gem update bundler
```

## Problems that were encountered and solved during the project

### 1) A new gem creation

### 2) Temporary file creation and operation on files

### 3) Preparing and running system commands (using open3 library)

### 4) Timer execution (timeout for the running commands)

### 5) Automatic tests
